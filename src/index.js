import React from 'react';
import ReactDOM from 'react-dom';
// import SelectSearch from 'react-select-search';
class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { stockName: '',
                  response:[],
                  stockList:[]}
  }
  mySubmitHandler = (event) => {
    event.preventDefault();
    const apiUrl = "https://mysterious-forest-51769.herokuapp.com/get?name=" + this.state.stockName;
    fetch(apiUrl,{method: "GET",mode: "cors"})
    .then(res=>res.json())
      .then(result => {
        this.setState({response: result})
      })
  }
  myChangeHandler = (event) => {
    this.setState({stockName: event.target.value});
    const apiUrl = "https://mysterious-forest-51769.herokuapp.com/list?name=" + this.state.stockName;
    fetch(apiUrl,{method: "GET",mode: "cors"})
    .then(res=>res.json())
      .then(result => {
        this.setState({stockList: result})
      })
  }
  render() {
    const res = this.state.response;
    const list = this.state.stockList;
    return (
      <table>
        <tr><td>
        <form onSubmit={this.mySubmitHandler}>
        <h2>The Easiest Way to buy and sell stocks</h2>
        <br></br>
        <p>Stock analysis and screening tool for investors in India</p>
        <br></br>
        <input
          type='text'
          onChange={this.myChangeHandler}
        />
        <input
          type='submit'
        />
        </form>
      </td></tr>
      <tr><td>
      {list.map(stock => (
        <p>{stock.name}</p>
      ))}
      </td></tr>
      <tr><td>
      {res.map(resp => (<div>
        <h3>{resp.name}</h3><br></br>
        <b>Market Cap: </b>{resp.currentMarketPrice}<br></br>
        <b>Current Price: </b>{resp.currentMarketPrice}<br></br>
        <b>Stock P/E: </b>{resp.stockPE} %<br></br>
        <b>Debt: </b>{resp.dept}<br></br>
        <b>Divident Yield: </b>{resp.dividendYield} %<br></br>
        <b>ROCE: </b>{resp.roce} %<br></br>
        <b>ROE: </b>{resp.ROEPreviousAnnum} %<br></br>
        <b>Debt To Equity: </b>{resp.debtToEquity} %<br></br>
        <b>Eps: </b>{resp.eps}<br></br>
        <b>Reserves: </b>{resp.reserves}<br></br>

      </div>
      
      ))}
      </td></tr>
      
      </table>
    );
  }
}

ReactDOM.render(<MyForm />, document.getElementById('root'));